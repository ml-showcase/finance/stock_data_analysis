# Stock Data Analysis

This project is abut basic analysis for stocks and the S&P500 from 12th January 2020 up to 11th August 2020.

See the [notebook](https://gitlab.com/ml-showcase/finance/stock_data_analysis/-/blob/master/stocks_data_analysis.ipynb), it includes:
- Statistical information
- Normalized (Scaled) plot
- Correlations
- Daily returns
- Histogram for daily returns

The stocks included in this project are:
- **AAPL** = Apple Stock
- **BA** = Boeing
- **T** = AT&T
- **MGM** = MGM Resorts International (Hotel Industry)
- **AMZN** = Amazon
- **IBM** = IBM
- **TSLA** = Tesla Motors
- **GOOG** = Google
- **sp500** = S&P 500 is a stock market index that measures the stock performance of 500 large companies listed on U.S. stock exchange. Check the list of S&P 500 companies [here](https://en.wikipedia.org/wiki/List_of_S%26P_500_companies).